<?php
/**
 * Created by PhpStorm.
 * User: abdirahim
 * Date: 22/04/2018
 * Time: 12:23
 */
use Michelf\MarkdownExtra;
use Carbon\Carbon;


class convertMarkDown {

    private $configs;


    public function __construct($configs){

        $this->configs = $configs;

        $nowInLondonTz = Carbon::now('Europe/London');

   $this->logEvent($nowInLondonTz);

   $file = 'markdown.md';

        if (filesize($file) > 1000000) {
            echo 'exceeded file limit';
        }

    $string = file_get_contents($file);

    $markdownParser = new MarkdownExtra();
    $content = $markdownParser->transform($string);


    $html = "<html>
                    <head>
                    </head>
                    <body>$content</body>
                    </html>";


    $this->saveHTML($html,$this->configs['storage']);

    }

    public function logEvent($message) {

        $file = 'log.txt';
        $current = file_get_contents($file);
        $current .= "$message\n";
        file_put_contents($file, $current);
    }

    public function saveHTML($html, $storageType='local') {

        if($storageType == 's3'){
            //do s3 storage
        } elseif ($storageType == 'sftp'){
            // do sftp storage
        } elseif ($storageType == 'local'){

            $file = 'htmlFile.html';
            $current = file_get_contents($file);
            $current .= "$html\n";
            file_put_contents($file, $current);

        }

    }

}